import React from 'react';
import {ClickedContaxt} from '../modules/Layout'

export default props => {
  return (
    <div style={{border: '1px solid #000000'}}>
      <h3>Counter2</h3>
      <ClickedContaxt.Consumer>
        {clicked => clicked ? <p>Cliced</p> : null}
      </ClickedContaxt.Consumer>
    </div>
  )

}