import React, { Component } from "react";
import PropTypes from 'prop-types';
import ReactDOM from "react-dom";
import Car from '../Car/Car.jsx'
import ErrorBoundary from "../ErrorBoundary/ErrorBoundary.jsx";
import Counter from "../Counter/Counter.jsx";

export const ClickedContaxt = React.createContext(false);


class Layout extends Component {
  constructor(props) {
    super(props)

    this.state = {
      cars: [
        {name: 'Ford', year: 2018},
        {name: 'Audi', year: 2016},
        {name: 'Mazda', year: 2010},
      ],
      pageTitle: 'React Component',
      showCars: false,
      clicked: false
    }
  }

  onChangeName(name, index){
      const car = this.state.cars[index];
      car.name = name;
      const cars = [...this.state.cars];
      cars[index] = car;
      this.setState({ cars });
    };

    toggleCarsHandler = () => {
      this.setState({
        showCars: !this.state.showCars,
      });
      console.log(this.state.showCars)
    };

  deleteHandler = index => {
    const cars = this.state.cars.concat();
    cars.splice(index, 1);
    this.setState({
      cars
    })
  };

  render() {
    let cars = null;

    if (this.state.showCars) {
      cars = this.state.cars.map((car, index) => {
        return (
          <ErrorBoundary key = {index}>
          <Car
            name = {car.name}
            index = {index}
            year = {car.year}
            onChangeName = {event => this.onChangeName(event.target.value, index)}
            onDelete = {() => this.deleteHandler(index)}
          />
            <ClickedContaxt.Provider value ={this.state.clicked}>
              <Counter/>
            </ClickedContaxt.Provider>
          </ErrorBoundary>)
      })
    }

     return (
       <div className={"wrapper"}>
         {/*<h1>{this.state.pageTitle}</h1>*/}
         <h1>{this.props.title}</h1>
         <button onClick={() => this.setState({clicked: ClickedContaxt}) }>Change click</button>
         <button onClick={this.toggleCarsHandler}>Toggle cars</button>
         <div style={{
           width: 400,
           margin: 'auto',
           paddingTop: '20px'
         }}>
         { cars }
       </div>
       </div>
     )
    }
}

export {Layout}