import React, {Component} from 'react';
import Auxilary from "../hoc/Auxililary";
import Cointer2 from "../Counter2/Cointer2";

export default class extends Component {
  state = {
    counter: 0,
  };

  counterPlus = () => {
    // this.setState({
    //   counter: this.state.counter + 1
    // })
    this.setState(prevState => {
      return (
        {
          counter: prevState.counter + 1
        }
      )
    })
  };

  counterMinus = () => {
    this.setState(prevState => {
      return (
        {
          counter: prevState.counter - 1
        }
      )
    })
  };

  render() {
    return (
      <React.Fragment>
        <h2>Counter {this.state.counter}</h2>
        <Cointer2/>
        <button onClick={this.counterPlus}>+</button>
        <button onClick={this.counterMinus}>-</button>
      </React.Fragment>
    )
  }
}